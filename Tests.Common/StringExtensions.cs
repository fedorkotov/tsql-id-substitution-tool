using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions ;

namespace Tests.Common
{
    public static class StringExtensions
    {
        private static readonly Regex REGEX_LINE_START_MARKER = 
            new Regex(@"^\s*\|(.*)", RegexOptions.Multiline);
        public static string TrimPipe(this string code)
        {
            return REGEX_LINE_START_MARKER.Replace(code, m => m.Groups[1].Value);
        }
    }
}
