using System;
using SqlParserAntlr;
using Antlr4.Runtime.Misc;
using Antlr4.Runtime;
using System.Collections.Generic;

namespace SQLNameSubstitution.TSQL
{
    public class TSQLListener : TSqlParserBaseListener
    {
        public List<IColumnReplacementSite> ColumnReplacementSites {get;}
            = new List<IColumnReplacementSite>();

        public List<ITableReplacementSite> TableReplacementSites {get;}
            = new List<ITableReplacementSite>();

        // TODO: separete replacement contexts for
        // different queries
        /*public override void EnterDml_clause(
            [NotNull] TSqlParser.Dml_clauseContext ctx)
        {

        }*/

        private IToken? _getTableNameToken([NotNull] TSqlParser.Table_source_itemContext ctx)
        {
            var fullTableName = ctx.full_table_name();
            if(fullTableName!=null)
            {
                return fullTableName.table.Start;
            }

            var tableNameWithHint = ctx.table_name_with_hint();
            if(tableNameWithHint!=null)
            {
                return tableNameWithHint.table_name().table.Start;
            }

            return null;
        }


        public override void EnterDdl_object(
            [NotNull] TSqlParser.Ddl_objectContext ctx)
        {
            var fullTableName = ctx.full_table_name();
            if(fullTableName!=null)
            {
                TableReplacementSite tableName =
                    new TableReplacementSite(
                        fullTableName.table.Start);

                TableReplacementSites.Add(tableName);
            }
        }

        public override void EnterTable_source_item(
            [NotNull] TSqlParser.Table_source_itemContext ctx)
        {
            var tableNameToken = _getTableNameToken(ctx);
            if(tableNameToken==null)
            {
                return;
            }

            TableReplacementSite tableName =
                new TableReplacementSite(
                    tableNameToken,
                    aliasToken:
                        ctx.as_table_alias()
                            ?.table_alias()
                            .id()
                            .Start);

            TableReplacementSites.Add(tableName);
        }

        public override void EnterFull_column_name(
            [NotNull] TSqlParser.Full_column_nameContext ctx)
        {
            var tableNameContext = ctx.table_name();
            IToken? tableToken = null;
            if(tableNameContext!=null)
            {
                tableToken = tableNameContext.table.Start;
            }

            ColumnReplacementSite columnName =
                new ColumnReplacementSite(
                    ctx.column_name.Start,
                    tableToken: tableToken);

            ColumnReplacementSites.Add(columnName);
        }

        public override void EnterColumn_name_list(
            [NotNull] TSqlParser.Column_name_listContext ctx)
        {
            foreach(var idContext in ctx.id())
            {
                ColumnReplacementSite columnName =
                new ColumnReplacementSite(
                    idContext.Start);

                ColumnReplacementSites.Add(columnName);
            }
        }

        public override void EnterColumn_elem([NotNull] TSqlParser.Column_elemContext ctx)
        {
            var tableNameContext = ctx.table_name();
            IToken? tableToken = null;
            if(tableNameContext!=null)
            {
                tableToken = tableNameContext.table.Start;
            }

            ColumnReplacementSite columnName =
                new ColumnReplacementSite(
                    ctx.column_name.Start,
                    tableToken: tableToken);

            ColumnReplacementSites.Add(columnName);
        }
    }
}
