using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Antlr4.Runtime;
using Antlr4.Runtime.Tree;
using SqlParserAntlr;

namespace SQLNameSubstitution.TSQL
{
    public class TSQLReplacementSitesExtractor : IReplacementSitesExtractor
    {
        public void GetReplacementSites(
            string query,
            out List<ITableReplacementSite> tableReplacementSites,
            out List<IColumnReplacementSite> columnReplacementSites)
        {
            GetReplacementSites(
                new MemoryStream(Encoding.UTF8.GetBytes(query)),
                out tableReplacementSites,
                out columnReplacementSites);
        }

        public void GetReplacementSites(
            Stream queryStream,
            out List<ITableReplacementSite> tableReplacementSites,
            out List<IColumnReplacementSite> columnReplacementSites)
        {
            ICharStream queryCharStream = CharStreams.fromStream(queryStream);
            queryCharStream = new CaseChangingCharStream(queryCharStream);
            ITokenSource lexer =
                new TSqlLexer(
                    queryCharStream,
                    TextWriter.Null,
                    TextWriter.Null);
            ITokenStream tokens = new CommonTokenStream(lexer);
            TSqlParser parser =
                new TSqlParser(tokens, TextWriter.Null, TextWriter.Null)
                {
                    BuildParseTree = true
                };
            IParseTree tree = parser.tsql_file();

            TSQLListener listener = new TSQLListener();

            ParseTreeWalker.Default.Walk(listener, tree);

            columnReplacementSites = listener.ColumnReplacementSites;
            tableReplacementSites = listener.TableReplacementSites;
        }
    }
}
