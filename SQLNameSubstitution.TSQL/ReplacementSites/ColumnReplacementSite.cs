using System;
using System.Text;
using SqlParserAntlr;
using Antlr4.Runtime.Misc;
using Antlr4.Runtime;
using System.Collections.Generic;

namespace SQLNameSubstitution.TSQL
{
    public class ColumnReplacementSite: IColumnReplacementSite
    {
        public ColumnReplacementSite(
            IToken columnNameToken,
            IToken? tableToken = null)
        {
            ColumnName = new IdentifierReplacementSite(columnNameToken);

            Table =
                tableToken == null?
                null:
                new IdentifierReplacementSite(tableToken);
        }

        public IIdentifierReplacementSite? Table {get;}

        public IIdentifierReplacementSite ColumnName {get;}

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            if(Table!=null)
            {
                sb.Append(Table);
                sb.Append(".");
            }

            sb.Append(ColumnName);

            return sb.ToString();
        }
    }
}