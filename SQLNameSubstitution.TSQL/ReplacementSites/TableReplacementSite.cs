using System;
using System.Text;
using SqlParserAntlr;
using Antlr4.Runtime.Misc;
using Antlr4.Runtime;
using System.Collections.Generic;

namespace SQLNameSubstitution.TSQL
{
    public class TableReplacementSite : ITableReplacementSite
    {
        public TableReplacementSite(
            IToken tableNameToken,
            IToken? aliasToken = null)
        {
            TableName = new IdentifierReplacementSite(tableNameToken);

            Alias =
                aliasToken!=null?
                new IdentifierReplacementSite(aliasToken):
                null;
        }

        public IIdentifierReplacementSite? Alias {get;}

        public IIdentifierReplacementSite TableName {get;}

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(TableName);

            if(Alias!=null)
            {
                sb.Append(" AS ");
                sb.Append(Alias);
            }

            return sb.ToString();
        }
    }
}