using System;
using SqlParserAntlr;
using Antlr4.Runtime.Misc;
using Antlr4.Runtime;

namespace SQLNameSubstitution.TSQL
{
    public class IdentifierReplacementSite: IIdentifierReplacementSite
    {
        private readonly IToken _token;

        public IdentifierReplacementSite(IToken token)
        {
            _token = token;
            OriginalName = _token.Text;

            if(OriginalName.StartsWith("\"") ||
               OriginalName.StartsWith("["))
            {
                OriginalUnquotedName =
                    OriginalName
                    .Substring(1, OriginalName.Length-2);
            }
            else
            {
                OriginalUnquotedName = OriginalName;
            }

            StartIdx = _token.StartIndex;
            Length = OriginalName.Length;
        }

        public int StartIdx {get;}

        public int Length {get;}

        [NotNull]
        public string OriginalName {get;}

        [NotNull]
        public string OriginalUnquotedName {get;}

        public override string ToString()
        {
            return $"[{OriginalUnquotedName}]";
        }

        public string GetDebugDescription()
        {
            return $"[{OriginalUnquotedName}] (start idx={StartIdx})";
        }
    }
}