using System;
using Xunit;
using Tests.Common;
using SQLNameSubstitution.TSQL;
using SQLNameSubstitution.DBModelLoader.YAML;
using System.Collections.Generic;
using System.Linq;

namespace SQLNameSubstitution.TSQLAdvanced.Tests
{
    public class SelectTests
    {
        // See Bug #2
        [Fact]
        public void FullTableNameInSelectColumnsList()
        {
            var dbModel =
                new YAMLDBModelLoader()
                .LoadDBModelFromString(
                    @"db-model-version: 1.0
                    |tables:
                    |  - name: Table1
                    |    name-replacement: TT1
                    |    columns:
                    |      - name: ID
                    |        name-replacement: TT1_ID
                    |      - name: Column1
                    |        name-replacement: TT1_c1
                    |  - name: Table2
                    |    name-replacement: TT2
                    |    columns:
                    |      - name: ID
                    |        name-replacement: TT2_ID
                    |      - name: Table1ID
                    |        name-replacement: TT2_TT1ID
                    |      - name: Column1
                    |        name-replacement: TT2_c1".TrimPipe());

            Assert.Equal(
                @"SELECT
                |    t1.TT1_c1,
                |    TT2.TT2_c1
                |FROM
                |    TT1 as t1
                |    INNER JOIN TT2 On TT2_TT1ID = t1.TT1_ID".TrimPipe(),
                new TSQLNameSubstitutionManager(
                    dbModel,
                    new TSQLReplacementSitesExtractor())
                .Replace(
                    @"SELECT
                    |    t1.Column1,
                    |    Table2.Column1
                    |FROM
                    |    table1 as t1
                    |    INNER JOIN Table2 On Table1ID = t1.ID".TrimPipe()));
        }

        // See Bug #7
        [Fact]
        public void LastQuoteInQueryShouldNotBeLost()
        {
            var dbModel =
                new YAMLDBModelLoader()
                .LoadDBModelFromString(
                    @"db-model-version: 1.0
                    |tables:
                    |  - name: Table1
                    |    name-replacement: TT1
                    |    columns:
                    |      - name: Column1
                    |        name-replacement: TT1_c1".TrimPipe());

            Assert.Equal(
                @"SELECT * FROM TT1 WHERE TT1_c1>'123456'".TrimPipe(),
                new TSQLNameSubstitutionManager(
                    dbModel,
                    new TSQLReplacementSitesExtractor())
                .Replace(
                    @"SELECT * FROM Table1 WHERE Column1>'123456'".TrimPipe()));
        }
    }
}
