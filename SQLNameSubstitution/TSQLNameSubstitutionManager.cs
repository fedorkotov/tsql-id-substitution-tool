using System;
using System.Text;
using System.IO;
using System.Collections.Generic;

namespace SQLNameSubstitution
{
    public class TSQLNameSubstitutionManager
    {
        private static readonly IEqualityComparer<string> ID_COMPARER =
            new TSQLIdentifierComparer();
        private readonly IDBSchema _dbModel;

        private readonly IReplacementSitesExtractor _replacementSitesExtractor;

        public TSQLNameSubstitutionManager(
            IDBSchema dbModel,
            IReplacementSitesExtractor replacementSitesExtractor)
        {
            _dbModel = dbModel;
            _replacementSitesExtractor = replacementSitesExtractor;
        }

        public string Replace(string query)
        {
            _replacementSitesExtractor
            .GetReplacementSites(
                query,
                out List<ITableReplacementSite> tableReplacementSites,
                out List<IColumnReplacementSite> columnReplacementSites);

            var queryMap =
                new TSQLQueryMap(
                    query,
                    ID_COMPARER,
                    tableReplacementSites.ConvertAll(x => (ITableReplacementSite)x),
                    columnReplacementSites);

            return
                queryMap.ReplaceNames(_dbModel);
        }
    }
}