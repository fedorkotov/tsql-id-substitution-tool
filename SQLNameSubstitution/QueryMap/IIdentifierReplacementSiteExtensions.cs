using System;

namespace SQLNameSubstitution
{
    public static class IIdentifierReplacementSiteExtensions
    {
        public static int GetEndIdx(this IIdentifierReplacementSite site)
            => site.StartIdx+site.Length-1;

        public static bool Overlaps(
            this IIdentifierReplacementSite site1,
            IIdentifierReplacementSite site2)
        {
            // https://stackoverflow.com/a/13513973/774130
            return
                site1.StartIdx <= site2.GetEndIdx() &&
                site2.StartIdx <= site1.GetEndIdx();
        }
    }
}
