using System;

namespace SQLNameSubstitution
{
    public interface ITableReplacementSite
    {
        IIdentifierReplacementSite? Alias {get;}

        IIdentifierReplacementSite TableName {get;}
    }
}
