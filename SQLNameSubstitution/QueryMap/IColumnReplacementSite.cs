using System;

namespace SQLNameSubstitution
{
    public interface IColumnReplacementSite
    {
        /// <summary>
        /// Optional table name or alias
        /// </summary>
        /// <value></value>
        IIdentifierReplacementSite? Table {get;}

        IIdentifierReplacementSite ColumnName {get;}
    }
}
