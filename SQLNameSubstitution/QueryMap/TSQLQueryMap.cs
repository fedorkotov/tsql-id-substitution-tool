using System;
using System.Linq;
using System.Collections.Generic;

namespace SQLNameSubstitution
{
    public class TSQLQueryMap
    {
        private readonly List<ITableReplacementSite> _tableReplacementSites;
        private readonly List<ColumnReplacementSiteWithCandidateTables> _columnReplacementSites;

        public TSQLQueryMap(
            string query,
            IEqualityComparer<string> idEqualityComparer,
            List<ITableReplacementSite> tableReplacementSites,
            IEnumerable<IColumnReplacementSite> columnReplacementSites)
        {
            Query = query;

            _tableReplacementSites = tableReplacementSites;

            _columnReplacementSites =
                columnReplacementSites
                .Select(
                    x => new ColumnReplacementSiteWithCandidateTables(
                            idEqualityComparer,
                            x,
                            _tableReplacementSites))
                .ToList();
        }

        public string Query {get;}

        private OrderedIdentifierReplacementSitesCollection _GetReplacements(
            IDBSchema dbScheme)
        {
            var replacements =
                new OrderedIdentifierReplacementSitesCollection();

            Dictionary<ITableReplacementSite, ITableNameSubstitution> _tableNameSubstitutionsByTableReplacementSite =
                new Dictionary<ITableReplacementSite, ITableNameSubstitution>(_tableReplacementSites.Count);

            foreach(var tableReplacementSite in _tableReplacementSites)
            {
                if(dbScheme.TryGetTable(
                    tableReplacementSite.TableName.OriginalUnquotedName,
                    out ITableNameSubstitution tableNameSubstitution))
                {
                    replacements.Add(
                        tableReplacementSite.TableName,
                        tableNameSubstitution.ReplacementName);

                    _tableNameSubstitutionsByTableReplacementSite[tableReplacementSite] =
                        tableNameSubstitution;
                }
            }

            foreach(var columnReplacementSite in _columnReplacementSites)
            {
                Dictionary<ITableNameSubstitution, INameSubstitution> matchingSubstitutions =
                    new Dictionary<ITableNameSubstitution, INameSubstitution>();

                foreach(var candidateTable in columnReplacementSite.TableCandidates)
                {
                    if(_tableNameSubstitutionsByTableReplacementSite
                      .TryGetValue(
                        candidateTable, 
                        out ITableNameSubstitution tableNameSubstitution))
                    {
                        if (!matchingSubstitutions.ContainsKey(tableNameSubstitution) &&
                            tableNameSubstitution.TryGetColumn(
                                columnReplacementSite
                                    .Column
                                    .ColumnName
                                    .OriginalUnquotedName,
                                out INameSubstitution newColumnNameSubstitution))
                        {
                            matchingSubstitutions[tableNameSubstitution] =
                                newColumnNameSubstitution;
                        }
                    }
                }

                switch(matchingSubstitutions.Count)
                {
                    case 0:
                        // TODO: warning
                        break;
                    case 1:
                        var matchingSubstitution = matchingSubstitutions.First();

                        replacements.Add(
                            columnReplacementSite.Column.ColumnName,
                            matchingSubstitution.Value.ReplacementName);

                        if(!columnReplacementSite.TableNameIsEmptyOrAlias &&
                           columnReplacementSite.Column.Table!=null)
                        {
                            replacements.Add(
                                columnReplacementSite.Column.Table,
                                matchingSubstitution.Key.ReplacementName);
                        }
                        break;
                    default:
                        string matchingSubstitutionsStr =
                            string.Join(
                                ",",
                                matchingSubstitutions
                                .Select(x => $"{x.Key}.{x.Value}"));
                        throw new AmbiguousColumnNameException(
                            $@"{columnReplacementSite.Column.ColumnName.GetDebugDescription()
                            } is ambiguous between {matchingSubstitutionsStr}");
                }
            }

            return replacements;
        }

        public string ReplaceNames(
            IDBSchema dbScheme)
        {
            var replacements =
                _GetReplacements(dbScheme);

            return replacements.Apply(Query);
        }
    }
}