using System;

namespace SQLNameSubstitution
{
    public interface IIdentifierReplacementSite
    {
        int StartIdx {get;}

        int Length {get;}

        string OriginalName {get;}

        string OriginalUnquotedName {get;}

        string GetDebugDescription();
    }
}
