1. For each table name replacement site find 
    matching table name substitution in dbScheme.
1.1 Any table name replacement for which substitution was
    not found remains as is.
1.2 Insort table replacement sites into replacements list               
1.3 Prepare a dictionary 
    table_name_or_alias -> (table replacement site, substitution)               
2. For each field replacement site with candidate tables
2.1 find respective (table replacement site, subst) 
    in the dictionary (see 1.3)
2.2 search field with matching name in all tables found at 2.1
2.2.1 found 0 -> warn, remains wihtout substitution
2.2.2 found 1 -> Ok, insort replacement into replacements list
2.2.3 found >1 -> error  