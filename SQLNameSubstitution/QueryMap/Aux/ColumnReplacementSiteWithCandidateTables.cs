using System;
using System.Collections.Generic;
using System.Linq;

namespace SQLNameSubstitution
{
    public class ColumnReplacementSiteWithCandidateTables
    {
        public ColumnReplacementSiteWithCandidateTables(
            IEqualityComparer<string> idEqualityComparer,
            IColumnReplacementSite column,
            List<ITableReplacementSite> tablesInContext)
        {
            Column = column;

            if(column.Table==null)
            {
                TableCandidates = tablesInContext;
                TableNameIsEmptyOrAlias = true;
            }
            else
            {
                TableCandidates =
                    _FindSingleTableOrThrow(
                        idEqualityComparer,
                        tablesInContext,
                        column.Table.OriginalUnquotedName,
                        out bool tableNameIsEmptyOrAlias);

                TableNameIsEmptyOrAlias = tableNameIsEmptyOrAlias;
            }
        }

        private IList<ITableReplacementSite> _FindSingleTableOrThrow(
            IEqualityComparer<string> idEqualityComparer,
            IEnumerable<ITableReplacementSite> tablesInContext,
            string tableName,
            out bool tableNameIsEmptyOrAlias)
        {
            List<(bool aliasMatch, bool nameMatch, ITableReplacementSite site)> tablesWithMatchingNameOrAlias =
                tablesInContext
                .Select(
                    x => (
                        x.Alias!=null &&
                        idEqualityComparer.Equals(
                            tableName,
                            x.Alias.OriginalUnquotedName),
                        idEqualityComparer.Equals(
                            tableName,
                            x.TableName.OriginalUnquotedName),
                        x))
                .Where(x => x.Item1 || x.Item2)
                // Only need to know if more than 1
                .Take(2)
                .ToList();

            switch(tablesWithMatchingNameOrAlias.Count)
            {
                case 0:
                    throw new UnknownTableNameOrAliasException(
                        $@"Encountered alias or table name '{tableName
                        }' not present elsewhere in the query");
                case 1:
                    tableNameIsEmptyOrAlias =
                        tablesWithMatchingNameOrAlias[0]
                        .aliasMatch;
                    return tablesWithMatchingNameOrAlias.ConvertAll(x => x.site);
                default:
                    throw new DuplicateTableNamesException(
                        $@"Found multiple tables named '{tableName
                        }' while looking for parent table of '{Column
                        }' column");
            }
        }

        public IColumnReplacementSite Column {get;}


        public bool TableNameIsEmptyOrAlias {get;}

        /// <summary>
        /// Readonly possible parent tables collection
        /// </summary>
        /// <value></value>
        public IList<ITableReplacementSite> TableCandidates {get;}
    }
}
