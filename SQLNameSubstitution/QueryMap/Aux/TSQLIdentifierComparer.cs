using System;
using System.Collections.Generic;

namespace SQLNameSubstitution
{
    public class TSQLIdentifierComparer: IEqualityComparer<string>
    {
        public bool Equals (string? x, string? y)
        {
            return
                x!=null &&
                y!=null &&
                string.Equals(x, y, StringComparison.OrdinalIgnoreCase);
        }

        public int GetHashCode (string obj)
        {
            return obj.GetHashCode();
        }
    }
}
