using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;

namespace SQLNameSubstitution
{

    /// <summary>
    /// Collection of identifier replacement sites with
    /// replacement strings. Can be applied to query string.
    /// </summary>
    public class OrderedIdentifierReplacementSitesCollection
    {
        private readonly SortedList<int, IdentifierReplacement> _replacements =
            new SortedList<int, IdentifierReplacement>();

        /// <summary>
        /// Adds replacement site with replacement string to the collection
        /// </summary>
        /// <remarks>Throws <see cref="OverlappingReplacementSitesException"/>
        /// if <see cref="site"/> overlaps with previously added site</remarks>
        /// <param name="site"></param>
        /// <param name="replacement"></param>
        public void Add(
            IIdentifierReplacementSite site,
            string replacement)
        {
            IdentifierReplacement newReplacement =
                new IdentifierReplacement(site, replacement);

            IdentifierReplacement conflictingReplacement =
                _replacements
               .Values
               .FirstOrDefault(
                    x => x.ConflictsWith(newReplacement));

            if(conflictingReplacement != null)
            {
                throw new OverlappingReplacementSitesException(
                    $@"Conflicting replacements: {conflictingReplacement
                    }; {newReplacement}");
            }

            _replacements.Add(
                newReplacement.Site.StartIdx,
                newReplacement);
        }

        private static void _AddNonSubstitutedFragment(
            StringBuilder sb,
            string query,
            int prevEndIdx,
            int currentStartIdx)
        {
            int nonSubstitutedFragmentLength =
                currentStartIdx - prevEndIdx;

            if(nonSubstitutedFragmentLength>0)
            {
                sb.Append(
                    query,
                    prevEndIdx,
                    nonSubstitutedFragmentLength);
            }
        }

        public string Apply(string query)
        {
            if(_replacements.Count == 0)
            {
                return query;
            }

            StringBuilder sb = new StringBuilder();

            int prevEndIdx = 0;
            foreach(var replacement in _replacements.Values)
            {
                _AddNonSubstitutedFragment(
                    sb,
                    query,
                    prevEndIdx,
                    replacement.Site.StartIdx);

                sb.Append(replacement.Replacement);

                prevEndIdx = replacement.Site.GetEndIdx() + 1;
            }

            _AddNonSubstitutedFragment(
                sb,
                query,
                prevEndIdx,
                query.Length);

            return sb.ToString();
        }
    }
}