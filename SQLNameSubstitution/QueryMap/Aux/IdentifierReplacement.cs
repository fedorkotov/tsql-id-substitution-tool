using System;

namespace SQLNameSubstitution
{
    public class IdentifierReplacement
    {
        public IdentifierReplacement(
            IIdentifierReplacementSite site,
            string replacement)
        {
            Site =
                site ??
                throw new ArgumentNullException(nameof(site));

            Replacement =
                replacement ??
                throw new ArgumentNullException(nameof(replacement));
        }

        public IIdentifierReplacementSite Site {get;}

        public string Replacement {get;}

        public bool ConflictsWith(IdentifierReplacement other)
        {
            return this.Site.Overlaps(other.Site);
        }

        public string GetDebugDescription()
        {
            return $"{Site.GetDebugDescription()} -> '{Replacement}'";
        }
    }
}
