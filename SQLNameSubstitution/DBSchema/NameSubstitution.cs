using System;

namespace SQLNameSubstitution
{
    public class NameSubstitution : INameSubstitution
    {
        public NameSubstitution(
            string originalName,
            string replacementName)
        {
            OriginalName =
                originalName ??
                throw new ArgumentNullException(nameof(originalName));
            ReplacementName =
                replacementName ??
                throw new ArgumentNullException(nameof(replacementName));
        }

        public string OriginalName {get;}

        public string ReplacementName {get;}

        public INameSubstitution Reverse()
        {
            return new NameSubstitution(ReplacementName, OriginalName);
        }

        public override string ToString()
        {
            return $"[{OriginalName}]";
        }
    }
}
