using System;

namespace SQLNameSubstitution
{
    public interface IDBSchema
    {
        bool TryGetTable(
            string originalName,
            out ITableNameSubstitution table);

        /// <summary>
        /// Returns reversed db schema
        /// </summary>
        /// <returns>Returns db schema with all substitution
        /// directions reversed</returns>
        IDBSchema Reverse();
    }
}
