using System;

namespace SQLNameSubstitution
{
    public interface INameSubstitution
    {
        string OriginalName {get;}

        string ReplacementName {get;}

        /// <summary>
        /// Returns reversed name substitution
        /// </summary>
        /// <returns>Returns reversed name substitution</returns>
        INameSubstitution Reverse();
    }
}
