using System;

namespace SQLNameSubstitution
{
    public interface ITableNameSubstitution
    {
        string OriginalName {get;}

        string ReplacementName {get;}

        bool TryGetColumn(
            string originalName,
            out INameSubstitution column);

        /// <summary>
        /// Returns table schema with all substitution
        /// directions reversed
        /// </summary>
        /// <returns>Returns table schema with all substitution
        /// directions reversed</returns>
        ITableNameSubstitution Reverse();
    }
}
