using System;
using System.Collections.Generic;
using System.Linq;

namespace SQLNameSubstitution
{
    public class TableNameSubstitution : ITableNameSubstitution
    {
        private readonly Dictionary<string, INameSubstitution> _columnsByOriginalLowercaseName;

        public TableNameSubstitution(
            string originalName,
            string replacementName,
            IEnumerable<INameSubstitution> columns)
        {
            OriginalName =
                originalName ??
                throw new ArgumentNullException(nameof(originalName));

            ReplacementName =
                replacementName ??
                throw new ArgumentNullException(nameof(replacementName));

            _columnsByOriginalLowercaseName =
                columns
                .GroupBy(x => x.OriginalName.ToLower())
                .Select(x =>
                {
                    if(x.Count()>1)
                    {
                        throw new DuplicateColumnNamesException(
                            $@"'{originalName}' table contains {x.Count()
                            } columns with the same name '{x.Key}'");
                    }
                    return (name: x.Key, column: x.First());
                })
                .ToDictionary(
                    x => x.name,
                    x => x.column);
        }

        public string OriginalName {get;}

        public string ReplacementName {get;}

        public bool TryGetColumn(
            string originalName,
            out INameSubstitution column)
        {
            return
                _columnsByOriginalLowercaseName
                .TryGetValue(
                    originalName.ToLower(),
                    out column);
        }

        public ITableNameSubstitution Reverse()
        {
            return new TableNameSubstitution(
                ReplacementName,
                OriginalName,
                _columnsByOriginalLowercaseName
                    .Values
                    .Select(x => x.Reverse()));
        }

        public override string ToString()
        {
            return $"[{OriginalName}]";
        }
    }
}
