using System;
using System.Collections.Generic;
using System.Linq;

namespace SQLNameSubstitution
{
    public class DBSchema : IDBSchema
    {
        private readonly Dictionary<string, ITableNameSubstitution> _tablesByOriginalLowercaseName;

        public DBSchema(
            IEnumerable<ITableNameSubstitution> tables)
        {
            _tablesByOriginalLowercaseName =
                tables
                .GroupBy(x => x.OriginalName.ToLower())
                .Select(x =>
                {
                    if(x.Count()>1)
                    {
                        throw new DuplicateTableNamesException(
                            $"Found {x.Count()} tables with the same name '{x.Key}'");
                    }
                    return (name: x.Key, table: x.First());
                })
                .ToDictionary(
                    x => x.name,
                    x => x.table);
        }

        public bool TryGetTable(string originalName, out ITableNameSubstitution table)
        {
            return
                _tablesByOriginalLowercaseName
                .TryGetValue(
                    originalName.ToLower(),
                    out table);
        }

        public IDBSchema Reverse()
        {
            return new DBSchema(
                _tablesByOriginalLowercaseName
                .Values
                .Select(x => x.Reverse()));
        }
    }
}
