using System;
using System.Collections.Generic;

namespace SQLNameSubstitution
{
    public interface IReplacementSitesExtractor
    {
        void GetReplacementSites(
            string query,
            out List<ITableReplacementSite> tableReplacementSites,
            out List<IColumnReplacementSite> columnReplacementSites);
    }
}
