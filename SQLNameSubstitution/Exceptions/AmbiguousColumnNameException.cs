using System;

namespace SQLNameSubstitution
{
    public class AmbiguousColumnNameException: Exception
    {
        public AmbiguousColumnNameException() : base() { }
        public AmbiguousColumnNameException(string message) : base(message) { }
        public AmbiguousColumnNameException(string message, System.Exception inner) : base(message, inner) { }
    }
}
