using System;

namespace SQLNameSubstitution
{
    public class UnknownTableNameOrAliasException: Exception
    {
        public UnknownTableNameOrAliasException() : base() { }
        public UnknownTableNameOrAliasException(string message) : base(message) { }
        public UnknownTableNameOrAliasException(string message, System.Exception inner) : base(message, inner) { }
    }
}
