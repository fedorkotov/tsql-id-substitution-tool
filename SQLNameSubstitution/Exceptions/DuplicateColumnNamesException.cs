using System;

namespace SQLNameSubstitution
{
    public class DuplicateColumnNamesException: Exception
    {
        public DuplicateColumnNamesException() : base() { }
        public DuplicateColumnNamesException(string message) : base(message) { }
        public DuplicateColumnNamesException(string message, System.Exception inner) : base(message, inner) { }
    }
}
