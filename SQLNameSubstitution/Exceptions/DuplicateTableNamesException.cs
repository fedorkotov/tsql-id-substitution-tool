using System;

namespace SQLNameSubstitution
{
    public class DuplicateTableNamesException: Exception
    {
        public DuplicateTableNamesException() : base() { }
        public DuplicateTableNamesException(string message) : base(message) { }
        public DuplicateTableNamesException(string message, System.Exception inner) : base(message, inner) { }
    }
}
