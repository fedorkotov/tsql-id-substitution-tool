using System;

namespace SQLNameSubstitution
{
    public class OverlappingReplacementSitesException: ArgumentException
    {
        public OverlappingReplacementSitesException() : base() { }
        public OverlappingReplacementSitesException(string message) : base(message) { }
        public OverlappingReplacementSitesException(string message, System.Exception inner) : base(message, inner) { }

        public OverlappingReplacementSitesException(string message, string paramName) : base(message, paramName)
        {
        }

        public OverlappingReplacementSitesException(string message, string paramName, Exception innerException) : base(message, paramName, innerException)
        {
        }
    }
}
