using Avalonia;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Markup.Xaml;
using TSQLNameSubstitutionGUI.ViewModels;
using TSQLNameSubstitutionGUI.Views;

namespace TSQLNameSubstitutionGUI
{
    public class App : Application
    {
        public override void Initialize()
        {
            AvaloniaXamlLoader.Load(this);
        }

        public override void OnFrameworkInitializationCompleted()
        {
            if (ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
            {
                var mainWindow = new MainWindow();
                desktop.MainWindow = mainWindow;
                desktop.MainWindow.DataContext = 
                    new MainWindowViewModel(mainWindow);
            }

            base.OnFrameworkInitializationCompleted();
        }
    }
}