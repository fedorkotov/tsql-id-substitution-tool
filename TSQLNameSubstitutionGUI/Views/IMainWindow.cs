using System;
using System.ComponentModel;

namespace TSQLNameSubstitutionGUI.Views
{
    public interface IMainWindow
    {
        event EventHandler<CancelEventArgs> Closing;

        event EventHandler Opened;
    }
}
