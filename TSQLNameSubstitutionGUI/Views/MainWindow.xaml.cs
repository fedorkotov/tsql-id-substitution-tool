using System.ComponentModel;
using System.Reflection;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using AvaloniaEdit.Highlighting;
using AvaloniaEdit.Highlighting.Xshd;

namespace TSQLNameSubstitutionGUI.Views
{
    public class MainWindow : Window, IMainWindow
    {
        private const string SQL_HIGHLIGHTING_RESOURCE_NAME =
            "TSQLNameSubstitutionGUI.Resources.sql.xshd";

        public MainWindow()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);

            AvaloniaEdit.TextEditor originalQueryEditor =
                this.Find<AvaloniaEdit.TextEditor>("origialQueryEditor");
            AvaloniaEdit.TextEditor transformedQueryEditor =
                this.Find<AvaloniaEdit.TextEditor>("transformedQueryEditor");

            using (var stream = Assembly
                                .GetExecutingAssembly()
                                .GetManifestResourceStream(SQL_HIGHLIGHTING_RESOURCE_NAME))
            {
                using (var reader = new System.Xml.XmlTextReader(stream))
                {
                    IHighlightingDefinition sqlHighlightingDefinition =
                        HighlightingLoader.Load(
                            reader,
                            HighlightingManager.Instance);

                    originalQueryEditor.SyntaxHighlighting = sqlHighlightingDefinition;
                    transformedQueryEditor.SyntaxHighlighting = sqlHighlightingDefinition;
                }
            }
        }
    }
}