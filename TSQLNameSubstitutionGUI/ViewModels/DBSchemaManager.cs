using System;
using System.IO;
using SQLNameSubstitution;
using SQLNameSubstitution.DBModelLoader.YAML;
using SQLNameSubstitution.TSQL;

namespace TSQLNameSubstitutionGUI.ViewModels
{
    public class DBSchemaManager
    {
        private static readonly TSQLReplacementSitesExtractor _replacementSitesExtractor =
            new TSQLReplacementSitesExtractor();

        public string? SchemaPath {get; private set;}

        private TSQLNameSubstitutionManager? _forwardSubstitutionManager;
        private TSQLNameSubstitutionManager? _reversedSubstitutionManager;

        public bool SchemaLoaded => _forwardSubstitutionManager!=null;

        public string? SchemaReversalError {get; private set;}

        public bool SchemaIsReversible => _reversedSubstitutionManager!=null;

        /// <summary>
        /// <para>Only 3 outcomes are possible</para>
        /// <para>
        /// <list>
        ///   <item>
        ///     <term>schema successfully loaded and reversed.</term>
        ///     <description>Schema!=null, ReversedSchema!=null, SchemaReversalError == null
        ///        method returns false, errorMessage==null</description>
        ///   </item>
        ///   <item>
        ///     <term>schema successfully loaded but is nor reversible</term>
        ///     <description>Schema!=null, ReversedSchema==null, SchemaReversalError != null
        ///      method returns false, errorMessage==null</description>
        ///   </item>
        ///   <item>
        ///     <term>schema not loaded. </term>
        ///     <description>Schema, ReversedSchema and SchemaReversalError not changed
        ///      method returns true, errorMessage!=null</description> </item>
        /// </list>
        /// </para></summary>
        /// <param name="dbSchemaFilePath"></param>
        /// <param name="errorMessage"></param>
        /// <returns>schema replaced</returns>
        public bool LoadSchema(string dbSchemaFilePath, out string? errorMessage)
        {
            if(!File.Exists(dbSchemaFilePath))
            {
                errorMessage = $"DB schema file '{dbSchemaFilePath}' does not exist";
                return false;
            }

            IDBSchema schema;
            try
            {
                schema =
                    new YAMLDBModelLoader()
                    .LoadDBModelFromFile(dbSchemaFilePath);

                _forwardSubstitutionManager =
                    new TSQLNameSubstitutionManager(
                        schema,
                        _replacementSitesExtractor);

                errorMessage = null;
            }
            catch(Exception ex)
            {
                errorMessage = $"Can not open db schema\n'{ex.Message}'";
                return false;
            }

            try
            {
                IDBSchema reversedSchema = schema.Reverse();
                _reversedSubstitutionManager =
                    new TSQLNameSubstitutionManager(
                        reversedSchema,
                        _replacementSitesExtractor);

                SchemaReversalError = null;
            }
            catch(Exception ex)
            {
                _reversedSubstitutionManager = null;
                SchemaReversalError =
                    $"DB schema can not be reversed\n'{ex.Message}'";
            }

            SchemaPath = dbSchemaFilePath;

            return true;
        }

        public bool TransformQuery(
            bool forward,
            string? query,
            out string? transformedQuery,
            out string? errorMessage)
        {
            if(forward)
            {
                return _TransformQuery(
                    _forwardSubstitutionManager,
                    query,
                    out transformedQuery,
                    out errorMessage);
            }
            else
            {
                if(!SchemaIsReversible)
                {
                    transformedQuery = null;
                    errorMessage = SchemaReversalError;
                    return false;
                }

                return _TransformQuery(
                    _reversedSubstitutionManager,
                    query,
                    out transformedQuery,
                    out errorMessage);
            }
        }

        private bool _TransformQuery(
            TSQLNameSubstitutionManager? substitutionManager,
            string? query,
            out string? transformedQuery,
            out string? errorMessage)
        {
            try
            {
                if(substitutionManager==null ||
                   string.IsNullOrWhiteSpace(query))
                {
                    transformedQuery = null;
                    errorMessage = null;
                    return true;
                }

                transformedQuery =
                    substitutionManager
                    .Replace(query);
                errorMessage = null;
                return true;
            }
            catch(Exception ex)
            {
                transformedQuery = null;
                errorMessage = "Can not transform query: " + ex.Message;
                return false;
            }
        }
    }
}
