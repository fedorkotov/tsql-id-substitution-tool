﻿using System;
using System.ComponentModel;
using System.Reactive;
using System.Reactive.Linq;
using System.Threading.Tasks;
using Avalonia.Controls;
using ReactiveUI;
using TSQLNameSubstitutionGUI.Configuration;
using TSQLNameSubstitutionGUI.Views;

namespace TSQLNameSubstitutionGUI.ViewModels
{
    public class MainWindowViewModel : ViewModelBase
    {
        enum ErrorMessageType
        {
            Info,
            Warning,
            Error
        }

        private readonly DBSchemaManager _dbSchemaManager = new DBSchemaManager();

        private readonly NameSubstitutionConfigManager _nameSubstitutionConfigManager =
            new NameSubstitutionConfigManager();
        private readonly OpenFileDialog _openSchemaFileDialog;

        private bool _doNotSaveConfig = false;
        private bool _doNotHandleOriginalQueryChanges = true;
        private bool _firstTransform = true;

        public MainWindowViewModel(IMainWindow mainWindow)
        {
            _openSchemaFileDialog = _InitializeOpenSchemaDialog();

            OpenSchemeCommand = ReactiveCommand.Create(_OnOpenSchemaFileBtnPressed);

            this.WhenAnyValue(
                    x => x.ForwardTransformationDirection)
                .Subscribe(
                    (x) => _TransformQuery(
                            x,
                            OriginalQuery));

            this.WhenAnyValue(x => x.OriginalQuery)
                .Throttle(
                    TimeSpan.FromMilliseconds(250),
                    RxApp.MainThreadScheduler)
                .Subscribe(
                    x => _TransformQuery(
                            ForwardTransformationDirection,
                            x));

            _ReadConfigurationFromFile();

            mainWindow.Closing += _OnWindowClosing;
            mainWindow.Opened += _OnWindowOpened;
        }

        private void _OnWindowOpened(object? sender, EventArgs args)
        {
            _doNotHandleOriginalQueryChanges = false;
        }

        private void _ShowErrorMessage(
            ErrorMessageType messageType,
            string message)
        {
            switch(messageType)
            {
                case ErrorMessageType.Error:
                    ErrorMessageHeader = "ERROR";
                    ErrorMessageColor = "Crimson";
                    break;
                case ErrorMessageType.Warning:
                    ErrorMessageHeader = "WARNING";
                    ErrorMessageColor = "Orange";
                    break;
                case ErrorMessageType.Info:
                    ErrorMessageHeader = "INFO";
                    ErrorMessageColor = "PowderBlue";
                    break;
            }

            ErrorMessage = message;
            ErrorMessageIsVisible = true;
        }

        private void DismissErrorMessage()
        {
            ErrorMessageIsVisible = false;
        }

        private async Task<string?> _GetSchemaPathFromUser()
        {
            string[] result =
                await _openSchemaFileDialog
                .ShowAsync(new Window());

            return
                result?.Length > 0 ?
                result[0]:
                null;
        }

        private OpenFileDialog _InitializeOpenSchemaDialog()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();

            openFileDialog.Filters.Add(
                new FileDialogFilter()
                {
                    Name = "YAML DB Scheme",
                    Extensions =  { "yml", "yaml" }
                });
            openFileDialog.Title = "DB schema file";
            openFileDialog.AllowMultiple = false;

            return openFileDialog;
        }

        public ReactiveCommand<Unit, Unit> OpenSchemeCommand { get; }

        private string? _transformedQuery;
        public string? TransformedQuery
        {
            get => _transformedQuery;
            set => this.RaiseAndSetIfChanged(ref _transformedQuery, value);
        }

        private string? _dbSchemaPath;
        public string? DbSchemaPath
        {
            get => _dbSchemaPath;
            set => this.RaiseAndSetIfChanged(ref _dbSchemaPath, value);
        }

        private string? _originalQuery;
        public string? OriginalQuery
        {
            get => _originalQuery;
            set => this.RaiseAndSetIfChanged(ref _originalQuery, value);
        }

        private string? _errorMessage;
        public string? ErrorMessage
        {
            get => _errorMessage;
            set => this.RaiseAndSetIfChanged(ref _errorMessage, value);
        }

        private string? _errorMessageHeader;
        public string? ErrorMessageHeader
        {
            get => _errorMessageHeader;
            set => this.RaiseAndSetIfChanged(ref _errorMessageHeader, value);
        }

        private string? _errorMessageColor;
        public string? ErrorMessageColor
        {
            get => _errorMessageColor;
            set => this.RaiseAndSetIfChanged(ref _errorMessageColor, value);
        }

        private bool _errorMessageIsVisible = false;
        public bool ErrorMessageIsVisible
        {
            get => _errorMessageIsVisible;
            set => this.RaiseAndSetIfChanged(ref _errorMessageIsVisible, value);
        }

        private bool _forwardTransformationDirection = true;
        public bool ForwardTransformationDirection
        {
            get => _forwardTransformationDirection;
            set => this.RaiseAndSetIfChanged(ref _forwardTransformationDirection, value);
        }

        private void _TransformQuery(bool forwardDirection, string? query)
        {
            // TODO: find more elegant solution
            // Without these checks "DB schema not loaded"
            // replaces configuration loading or schema loading
            // error when the app starts
            if(_doNotHandleOriginalQueryChanges)
            {
                return;
            }
            if(_firstTransform)
            {
                _firstTransform = false;
                return;
            }

            if(!_dbSchemaManager.SchemaLoaded)
            {
                _ShowErrorMessage(
                    ErrorMessageType.Warning,
                    "DB schema not loaded");
                return;
            }

            if(_dbSchemaManager.TransformQuery(
                forwardDirection,
                query,
                out string? transformedQuery,
                out string? errorMessage))
            {
                TransformedQuery = transformedQuery??string.Empty;
                DismissErrorMessage();
            }
            else
            {
                TransformedQuery = string.Empty;
                if(errorMessage!=null)
                {
                    _ShowErrorMessage(
                        ErrorMessageType.Warning,
                        errorMessage);
                }
                else
                {
                    _ShowErrorMessage(
                        ErrorMessageType.Warning,
                        "Query can not be transformed "+
                        "for unknown reason");
                }
            }
        }

        async void _OnOpenSchemaFileBtnPressed()
        {
            string? schemaPath =
                await _GetSchemaPathFromUser()
                .ConfigureAwait(false);

            if(schemaPath!=null)
            {
                _OpenSchemaFile(schemaPath);
            }
        }

        private void _OnWindowClosing(object? sender, CancelEventArgs e)
        {
            if(!_doNotSaveConfig)
            {
                if(!_SaveConfigurationToFile())
                {
                    // If configuration was not saved
                    // windows closing should be prevented.
                    // Otherwise user would not be able to see
                    // error message
                    e.Cancel = true;
                }
            }
        }

        private bool _SaveConfigurationToFile()
        {
            try
            {
                _nameSubstitutionConfigManager.WriteConfiguration(
                    new NameSubstitutionToolConfiguration(
                        DbSchemaPath,
                        ForwardTransformationDirection));
                return true;
            }
            catch (ConfigIOException ex)
            {
                _doNotSaveConfig = true;
                _ShowErrorMessage(
                    ErrorMessageType.Error,
                    "Config saving error. " + ex.Message);
                return false;
            }
        }

        private void _ReadConfigurationFromFile()
        {
            try
            {
                var config = _nameSubstitutionConfigManager.ReadConfiguration();
                ForwardTransformationDirection = config.ForwardTransformation;
                if (config.DBSchemePath != null)
                {
                    _OpenSchemaFile(config.DBSchemePath);
                }
            }
            catch (ConfigIOException ex)
            {
                _doNotSaveConfig = true;
                _ShowErrorMessage(
                    ErrorMessageType.Error,
                    ex.Message);
            }
        }

        private void _OpenSchemaFile(string schemaPath)
        {
            if (_dbSchemaManager.LoadSchema(schemaPath, out string? errorMessage))
            {
                DbSchemaPath = _dbSchemaManager.SchemaPath;
                if (ForwardTransformationDirection)
                {
                    DismissErrorMessage();
                }
                else if (!_dbSchemaManager.SchemaIsReversible)
                {
                    TransformedQuery = string.Empty;
                    _ShowErrorMessage(
                        ErrorMessageType.Warning,
                        _dbSchemaManager.SchemaReversalError??
                            "Schema is not reversible for unknown reason");
                }
            }
            else
            {
                TransformedQuery = string.Empty;
                _ShowErrorMessage(
                    ErrorMessageType.Error,
                    errorMessage??
                        "Schema could not be loaded for unknown reason");
            }
        }
    }
}
