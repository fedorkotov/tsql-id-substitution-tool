using System;
using YamlDotNet.Serialization;

namespace TSQLNameSubstitutionGUI.Configuration
{
    public class YAMLConfigModel
    {
        [YamlMember(Alias = "config-version")]
        public string? Version {get;set;}

        public string? SchemePath {get; set;}

        public string? DefaultDirection {get; set;}
    }
}
