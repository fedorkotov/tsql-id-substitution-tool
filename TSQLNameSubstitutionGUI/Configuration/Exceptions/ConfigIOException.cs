using System;

namespace TSQLNameSubstitutionGUI.Configuration
{
    public abstract class ConfigIOException: Exception
    {
        public ConfigIOException() : base()
        {
        }

        public ConfigIOException(string message) : base(message)
        {
        }

        public ConfigIOException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
