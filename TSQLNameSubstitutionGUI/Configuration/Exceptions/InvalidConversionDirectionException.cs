using System;

namespace TSQLNameSubstitutionGUI.Configuration
{
    public class InvalidConversionDirectionException: ConfigIOException
    {
        public InvalidConversionDirectionException() : base()
        {
        }

        public InvalidConversionDirectionException(string message) : base(message)
        {
        }

        public InvalidConversionDirectionException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
