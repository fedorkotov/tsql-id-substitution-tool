using System;

namespace TSQLNameSubstitutionGUI.Configuration
{
    public class InvalidConfigVersionException: ConfigIOException
    {
        public InvalidConfigVersionException() : base()
        {
        }

        public InvalidConfigVersionException(string message) : base(message)
        {
        }

        public InvalidConfigVersionException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
