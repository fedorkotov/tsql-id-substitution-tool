using System;

namespace TSQLNameSubstitutionGUI.Configuration
{
    public class YamlConfigWritingExcaption: ConfigIOException
    {
        public YamlConfigWritingExcaption() : base()
        {
        }

        public YamlConfigWritingExcaption(string message) : base(message)
        {
        }

        public YamlConfigWritingExcaption(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
