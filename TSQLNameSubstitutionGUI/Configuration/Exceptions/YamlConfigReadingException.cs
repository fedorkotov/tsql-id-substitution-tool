using System;

namespace TSQLNameSubstitutionGUI.Configuration
{
    public class YamlConfigReadingException: ConfigIOException
    {
        public YamlConfigReadingException() : base()
        {
        }

        public YamlConfigReadingException(string message) : base(message)
        {
        }

        public YamlConfigReadingException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
