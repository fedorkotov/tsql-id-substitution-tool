using System;

namespace TSQLNameSubstitutionGUI.Configuration
{
    public class NameSubstitutionToolConfiguration
    {
        public NameSubstitutionToolConfiguration(
            string? dBSchemePath,
            bool forwardTransformation)
        {
            DBSchemePath = dBSchemePath;
            ForwardTransformation = forwardTransformation;
        }

        public string? DBSchemePath {get;}

        public bool ForwardTransformation {get;}
    }
}
