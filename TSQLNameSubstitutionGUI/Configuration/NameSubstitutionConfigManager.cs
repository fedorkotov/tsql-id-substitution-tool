using System;
using System.IO;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;

namespace TSQLNameSubstitutionGUI.Configuration
{
    public class NameSubstitutionConfigManager
    {
        public const string SUPPORTED_MODEL_VERSION = "1.0";
        private const string APPLICATION_FOLDER_NAME = "TSQLNameSubstitutionTool";
        private const string CONFIG_FILE_NAME = "config.yml";

        public const bool DEFAULT_FORWARD_DIRECTION = true;

        public const string FORWARD_DIRECTION_NAME = "forward";
        public const string BACKWARD_DIRECTION_NAME = "backward";


        public NameSubstitutionToolConfiguration ReadConfiguration(
            string? configPath=null)
        {
            string effectiveConfigPath =
                configPath ?? _GetDefaultConfigFilePath();

            if(!File.Exists(effectiveConfigPath))
            {
                return GetDefaultConfiguration();
            }

            YAMLConfigModel yamlConfig =
                _ReadYAMLConfigFromFile(effectiveConfigPath);

            if(yamlConfig.Version != SUPPORTED_MODEL_VERSION)
            {
                throw new InvalidConfigVersionException(
                    $@"Application config file '{effectiveConfigPath
                    }' has unsupported version '{yamlConfig.Version}'");
            }

            return new NameSubstitutionToolConfiguration(
                string.IsNullOrWhiteSpace(yamlConfig.SchemePath)?
                    null:
                    yamlConfig.SchemePath,
                _IsForwardDirection(yamlConfig.DefaultDirection));
        }

        public void WriteConfiguration(
            NameSubstitutionToolConfiguration config,
            string? configPath=null)
        {
            YAMLConfigModel yamlConfig =
                new YAMLConfigModel()
                {
                    Version = SUPPORTED_MODEL_VERSION,
                    SchemePath = config.DBSchemePath,
                    DefaultDirection =
                        _GetDirectionName(
                            config.ForwardTransformation)
                };
            string effectiveConfigPath =
                configPath ?? _GetDefaultConfigFilePath();

            if(!File.Exists(effectiveConfigPath))
            {
                string? configDirectory = Path.GetDirectoryName(effectiveConfigPath);
                if(!string.IsNullOrEmpty(configDirectory))
                {
                    try
                    {
                        Directory.CreateDirectory(configDirectory);
                    }
                    catch(Exception ex)
                    {
                        throw new YamlConfigWritingExcaption(
                            $"Unable to create config directory '{configDirectory}'",
                            ex);
                    }
                }
            }

            _WriteYAMLConfigToFile(
                effectiveConfigPath,
                yamlConfig);
        }

        private string _GetDefaultConfigFilePath()
        {
            return
                Path.Join(
                    Environment.GetFolderPath(
                        Environment.SpecialFolder.ApplicationData),
                    APPLICATION_FOLDER_NAME,
                    CONFIG_FILE_NAME);
        }

        public NameSubstitutionToolConfiguration GetDefaultConfiguration()
        {
            return
                new NameSubstitutionToolConfiguration(
                    null,
                    DEFAULT_FORWARD_DIRECTION);
        }

        private string _GetDirectionName(bool isForwardDirection)
        {
            return
                isForwardDirection?
                FORWARD_DIRECTION_NAME:
                BACKWARD_DIRECTION_NAME;
        }

        /// <summary>
        /// Decodes direction name or returns default conversion
        /// direction;
        /// </summary>
        /// <param name="direction"></param>
        /// <returns></returns>
        private bool _IsForwardDirection(string? direction)
        {
            if(string.IsNullOrWhiteSpace(direction))
            {
                return DEFAULT_FORWARD_DIRECTION;
            }
            else
            {
                var lowercaseDirection = direction.ToLower();
                if(lowercaseDirection == FORWARD_DIRECTION_NAME)
                {
                    return true;
                }
                else if(lowercaseDirection == BACKWARD_DIRECTION_NAME)
                {
                    return false;
                }
                else
                {
                    throw new InvalidConversionDirectionException(
                        $@"'{direction}' is not a valid conversion direction. Expected one of: {
                            FORWARD_DIRECTION_NAME}, {BACKWARD_DIRECTION_NAME}");
                }
            }
        }

        private YAMLConfigModel _ReadYAMLConfigFromFile(string filePath)
        {
            var deserializer =
                new DeserializerBuilder()
                .WithNamingConvention(HyphenatedNamingConvention.Instance)
                .Build();

            try
            {
                using(var input = File.OpenText(filePath))
                {
                    return deserializer.Deserialize<YAMLConfigModel>(
                        input);
                }
            }
            catch(Exception ex)
            {
                throw new YamlConfigReadingException(
                    $"Unable to read YAML config file '{filePath}'",
                    ex);
            }
        }

        private void _WriteYAMLConfigToFile(
            string filePath,
            YAMLConfigModel yamlConfig)
        {
            var serializer =
                new SerializerBuilder()
                .WithNamingConvention(HyphenatedNamingConvention.Instance)
                .Build();

            try
            {
                using var f = new StreamWriter(filePath);
                serializer.Serialize(f, yamlConfig);
            }
            catch(Exception ex)
            {
                throw new YamlConfigWritingExcaption(
                    $"Unable to write config file '{filePath}'",
                    ex);
            }
        }
    }
}