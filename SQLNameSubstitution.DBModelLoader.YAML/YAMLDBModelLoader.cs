﻿using System;
using System.IO;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;

namespace SQLNameSubstitution.DBModelLoader.YAML
{
    public class YAMLDBModelLoader
    {
        public const string SUPPORTED_MODEL_VERSION = "1.0";

        public IDBSchema LoadDBModelFromFile(
            string filePath)
        {
            var yamlDBModel = _ReadYAMLDbModelFromFile(filePath);
            _CheckModelVersion(yamlDBModel);

            return yamlDBModel.ToTableNameSubstitution();
        }

        public IDBSchema LoadDBModelFromString(
            string yamlString)
        {
            var yamlDBModel = _ReadYAMLDbModelFromString(yamlString);
            _CheckModelVersion(yamlDBModel);

            return yamlDBModel.ToTableNameSubstitution();
        }

        private static void _CheckModelVersion(YAMLDBModel yamlDBModel)
        {
            if (string.IsNullOrWhiteSpace(yamlDBModel.Version))
            {
                throw new DBModelVertionNotSpecifiedException(
                    "Missing db model version");
            }

            if (yamlDBModel.Version != SUPPORTED_MODEL_VERSION)
            {
                throw new DBModelVertionNotSupportedException(
                    $@"Unsupported DB model version '{yamlDBModel.Version
                    }'. Expected '{SUPPORTED_MODEL_VERSION}'");
            }
        }

        private YAMLDBModel _ReadYAMLDbModelFromString(string yamlString)
        {
            IDeserializer deserializer = _ConfigureDeserializer();

            return deserializer.Deserialize<YAMLDBModel>(yamlString);
        }

        private static IDeserializer _ConfigureDeserializer()
        {
            return
                new DeserializerBuilder()
                .WithNamingConvention(HyphenatedNamingConvention.Instance)
                .Build();
        }

        private YAMLDBModel _ReadYAMLDbModelFromFile(string filePath)
        {
            var deserializer = _ConfigureDeserializer();

            using(var input = File.OpenText(filePath))
            {
                return deserializer.Deserialize<YAMLDBModel>(input);
            }
        }
    }
}
