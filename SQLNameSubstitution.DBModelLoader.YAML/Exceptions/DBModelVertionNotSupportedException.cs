using System;
using System.Runtime.Serialization;

namespace SQLNameSubstitution.DBModelLoader.YAML
{
    public class DBModelVertionNotSupportedException : Exception
    {
        public DBModelVertionNotSupportedException()
        {
        }

        public DBModelVertionNotSupportedException(string message) : base(message)
        {
        }

        public DBModelVertionNotSupportedException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
