using System;
using System.Runtime.Serialization;

namespace SQLNameSubstitution.DBModelLoader.YAML
{
    public class DBModelVertionNotSpecifiedException : Exception
    {
        public DBModelVertionNotSpecifiedException()
        {
        }

        public DBModelVertionNotSpecifiedException(string message) : base(message)
        {
        }

        public DBModelVertionNotSpecifiedException(string message, Exception innerException) : base(message, innerException)
        {
        }        
    }
}
