using System;
using System.Runtime.Serialization;

namespace SQLNameSubstitution.DBModelLoader.YAML
{
    public class MissingColumnNameException : Exception
    {
        public MissingColumnNameException()
        {
        }

        public MissingColumnNameException(string message) : base(message)
        {
        }

        public MissingColumnNameException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
