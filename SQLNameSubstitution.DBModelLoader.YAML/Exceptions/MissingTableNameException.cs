using System;
using System.Runtime.Serialization;

namespace SQLNameSubstitution.DBModelLoader.YAML
{
    public class MissingTableNameException : Exception
    {
        public MissingTableNameException()
        {
        }

        public MissingTableNameException(string message) : base(message)
        {
        }

        public MissingTableNameException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
