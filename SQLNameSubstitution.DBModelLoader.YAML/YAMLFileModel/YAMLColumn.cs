using System;
using YamlDotNet.Serialization;

namespace SQLNameSubstitution.DBModelLoader.YAML
{
    internal class YAMLColumn
    {
        public string? Name {get;set;}

        public string? NameReplacement {get;set;}


        public INameSubstitution ToColumnNameSubstitution()
        {
            if(string.IsNullOrWhiteSpace(Name))
            {
                throw new MissingColumnNameException(
                    "Column name is missing");
            }

            var trimmedName = Name.Trim();

            return
                new NameSubstitution(
                    trimmedName,
                    string.IsNullOrWhiteSpace(NameReplacement) ?
                        trimmedName :
                        NameReplacement.Trim());
        }
    }
}
