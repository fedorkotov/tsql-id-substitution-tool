using System;
using System.Collections.Generic;
using System.Linq;
using YamlDotNet.Serialization;

namespace SQLNameSubstitution.DBModelLoader.YAML
{
    internal class YAMLTable
    {
        public string? Name {get;set;}

        public string? NameReplacement {get;set;}

        public List<YAMLColumn> Columns {get;set;} = new List<YAMLColumn>();

        public ITableNameSubstitution ToTableNameSubstitution()
        {
            if(string.IsNullOrWhiteSpace(Name))
            {
                throw new MissingTableNameException(
                    "Table name is missing");
            }

            var trimmedName = Name.Trim();

            return
                new TableNameSubstitution(
                    trimmedName,
                    string.IsNullOrWhiteSpace(NameReplacement) ?
                        trimmedName :
                        NameReplacement.Trim(),
                    Columns.Select(x => x.ToColumnNameSubstitution()));
        }
    }
}
