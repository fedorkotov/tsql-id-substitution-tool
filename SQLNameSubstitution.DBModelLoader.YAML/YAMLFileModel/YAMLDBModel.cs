using System;
using System.Collections.Generic;
using System.Linq;
using YamlDotNet.Serialization;

namespace SQLNameSubstitution.DBModelLoader.YAML
{
    internal class YAMLDBModel
    {
        [YamlMember(Alias = "db-model-version")]
        public string? Version {get;set;}

        public List<YAMLTable> Tables {get;set;} = new List<YAMLTable>();

        public IDBSchema ToTableNameSubstitution()
        {
            return
                new DBSchema(
                    Tables.Select(x => x.ToTableNameSubstitution()));
        }
    }
}
