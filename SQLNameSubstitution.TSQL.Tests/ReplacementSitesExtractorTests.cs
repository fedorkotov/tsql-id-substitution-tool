using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;
using Tests.Common;

namespace SQLNameSubstitution.TSQL.Tests
{
    public class ReplacementSitesExtractorTests
    {
        [Fact]
        public void SimpleSelect()
        {
            const string query = "SELECT c1, c2 FROM [Table1] as t1";

            new TSQLReplacementSitesExtractor()
            .GetReplacementSites(
                query,
                out List<ITableReplacementSite> tableReplacementSites,
                out List<IColumnReplacementSite> columnReplacementSites);

            Assert.Single(tableReplacementSites);

            var tableReplacementSite = tableReplacementSites[0];
            Assert.Equal(
                query.IndexOf("[Table1]"),
                tableReplacementSite.TableName.StartIdx);
            Assert.Equal(
                "[Table1]",
                tableReplacementSite.TableName.OriginalName);
            Assert.Equal(
                "Table1",
                tableReplacementSite.TableName.OriginalUnquotedName);

            Assert.NotNull(tableReplacementSite.Alias);
            #pragma warning disable CS8602
            Assert.Equal(
                query.IndexOf("t1"),
                tableReplacementSite.Alias.StartIdx);
            Assert.Equal(
                "t1",
                tableReplacementSite.Alias.OriginalName);
            #pragma warning restore CS8602

            Assert.Equal(2, columnReplacementSites.Count);

            Assert.Contains(
                columnReplacementSites,
                x => x.ColumnName.OriginalName == "c1" &&
                     x.Table == null);

            Assert.Contains(
                columnReplacementSites,
                x => x.ColumnName.OriginalName == "c2" &&
                     x.Table == null);
        }

        [Fact]
        public void SimpleUpdate()
        {
            const string query = "UPDATE Table1 SET c1=1, c2=c1 WHERE c1=2";

            new TSQLReplacementSitesExtractor()
            .GetReplacementSites(
                query,
                out List<ITableReplacementSite> tableReplacementSites,
                out List<IColumnReplacementSite> columnReplacementSites);

            Assert.Single(tableReplacementSites);

            var tableReplacementSite = tableReplacementSites[0];
            Assert.Equal(
                query.IndexOf("Table1"),
                tableReplacementSite.TableName.StartIdx);
            Assert.Equal(
                "Table1",
                tableReplacementSite.TableName.OriginalName);
            Assert.Equal(
                "Table1",
                tableReplacementSite.TableName.OriginalUnquotedName);

            Assert.Equal(4, columnReplacementSites.Count);

            Assert.Equal(
                3,
                columnReplacementSites
                .Count(
                    x => x.ColumnName.OriginalName == "c1"));

            Assert.Single(
                columnReplacementSites
                .Where(
                    x => x.ColumnName.OriginalName == "c2"));
        }

        [Fact]
        public void SimpleInsert()
        {
            const string query = "INSERT INTO Table1(c1, c2) VALUES (123, @v1)";

            new TSQLReplacementSitesExtractor()
            .GetReplacementSites(
                query,
                out List<ITableReplacementSite> tableReplacementSites,
                out List<IColumnReplacementSite> columnReplacementSites);

            Assert.Single(tableReplacementSites);

            var tableReplacementSite = tableReplacementSites[0];
            Assert.Equal(
                query.IndexOf("Table1"),
                tableReplacementSite.TableName.StartIdx);
            Assert.Equal(
                "Table1",
                tableReplacementSite.TableName.OriginalName);
            Assert.Equal(
                "Table1",
                tableReplacementSite.TableName.OriginalUnquotedName);

            Assert.Equal(2, columnReplacementSites.Count);

            Assert.Single(
                columnReplacementSites
                .Where(
                    x => x.ColumnName.OriginalName == "c1"));

            Assert.Single(
                columnReplacementSites
                .Where(
                    x => x.ColumnName.OriginalName == "c2"));
        }

        [Fact]
        public void InsertFromSelect()
        {
            const string query =
                    "INSERT INTO Table1(c1, c2) "+
                    "SELECT c1, c2 FROM [Table2] WHERE c3 > 'abc'";

            new TSQLReplacementSitesExtractor()
            .GetReplacementSites(
                query,
                out List<ITableReplacementSite> tableReplacementSites,
                out List<IColumnReplacementSite> columnReplacementSites);

            Assert.Equal(2, tableReplacementSites.Count);

            Assert.Single(
                tableReplacementSites
                .Where(
                    x => x.TableName.OriginalName == "Table1"));

            Assert.Single(
                tableReplacementSites
                .Where(
                    x => x.TableName.OriginalUnquotedName == "Table2"));

            Assert.Equal(5, columnReplacementSites.Count);

            Assert.Equal(
                2,
                columnReplacementSites
                .Count(
                    x => x.ColumnName.OriginalName == "c1"));

            Assert.Equal(
                2,
                columnReplacementSites
                .Count(
                    x => x.ColumnName.OriginalName == "c2"));

            Assert.Single(
                columnReplacementSites
                .Where(
                    x => x.ColumnName.OriginalName == "c3"));
        }

        [Fact]
        public void SimpleDelete()
        {
            const string query = "DELETE [Table1] WHERE c1 > 7";

            new TSQLReplacementSitesExtractor()
            .GetReplacementSites(
                query,
                out List<ITableReplacementSite> tableReplacementSites,
                out List<IColumnReplacementSite> columnReplacementSites);

            Assert.Single(tableReplacementSites);

            var tableReplacementSite = tableReplacementSites[0];
            Assert.Equal(
                query.IndexOf("[Table1]"),
                tableReplacementSite.TableName.StartIdx);
            Assert.Equal(
                "[Table1]",
                tableReplacementSite.TableName.OriginalName);
            Assert.Equal(
                "Table1",
                tableReplacementSite.TableName.OriginalUnquotedName);

            Assert.Single(columnReplacementSites);

            Assert.Single(
                columnReplacementSites
                .Where(
                    x => x.ColumnName.OriginalName == "c1"));
        }        
    }
}
