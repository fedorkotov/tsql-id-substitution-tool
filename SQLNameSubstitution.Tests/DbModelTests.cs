using System;
using System.Collections.Generic;
using Xunit;

namespace SQLNameSubstitution.Tests
{
    public class DbModelTests
    {
        [Fact]
        public void DuplicateColumnNames()
        {
            Assert.Throws<DuplicateColumnNamesException>(
                () =>
                {
                    new TableNameSubstitution(
                        "Table1",
                        "TT1",
                        new List<NameSubstitution>()
                        {
                            new NameSubstitution("ID", "TT1_ID"),
                            new NameSubstitution("Column1", "TT1_Column1"),
                            new NameSubstitution("ID", "TT1_ID")
                        });
                });
        }

        [Fact]
        public void DuplicateTableNames()
        {
            Assert.Throws<DuplicateTableNamesException>(
                () =>
                {
                    new DBSchema(
                        new List<ITableNameSubstitution>()
                        {
                            new TableNameSubstitution(
                                "Table1",
                                "TT2",
                                new List<NameSubstitution>()),
                            new TableNameSubstitution(
                                "Table2",
                                "TT2",
                                new List<NameSubstitution>()),
                            new TableNameSubstitution(
                                "Table1",
                                "TT1",
                                new List<NameSubstitution>())
                        });
                });
        }

        [Fact]
        public void CanFindTable()
        {
            var dbModel =
                new DBSchema(
                    new List<ITableNameSubstitution>()
                    {
                        new TableNameSubstitution(
                            "Table1",
                            "TT2",
                            new List<NameSubstitution>()),
                        new TableNameSubstitution(
                            "Table2",
                            "TT2",
                            new List<NameSubstitution>())
                    });

            Assert.True(dbModel.TryGetTable(
                "Table1",
                out ITableNameSubstitution table1));
            Assert.Equal("Table1", table1.OriginalName);

            Assert.True(dbModel.TryGetTable(
                "Table2",
                out ITableNameSubstitution table2));
            Assert.Equal("Table2", table2.OriginalName);
        }

        [Fact]
        public void TableName()
        {
            var table1 =
                new TableNameSubstitution(
                    "Table1",
                    "TT1",
                    new List<NameSubstitution>());

            Assert.Equal(
                "Table1",
                table1.OriginalName);
            Assert.Equal(
                "TT1",
                table1.ReplacementName);
        }

        [Fact]
        public void CanFindColumn()
        {
            var table1 =
                new TableNameSubstitution(
                    "Table1",
                    "TT1",
                    new List<NameSubstitution>()
                    {
                        new NameSubstitution("ID", "TT1_ID"),
                        new NameSubstitution("Column1", "TT1_Column1")
                    });

            Assert.True(table1.TryGetColumn(
                "ID",
                out INameSubstitution column1));
            Assert.Equal("ID", column1.OriginalName);

            Assert.True(table1.TryGetColumn(
                "Column1",
                out INameSubstitution column2));
            Assert.Equal("Column1", column2.OriginalName);
        }
    }
}
