using System;
using System.Collections.Generic;
using System.Linq;

namespace SQLNameSubstitution.Tests
{
    public static class TestTools
    {
        public static TableReplacementSiteMockup GetTableWithoutAlias(
            int startIdx,
            string tableName)
        {
            return
                new TableReplacementSiteMockup(
                    new IdentifierReplacementSiteMockup(
                        startIdx+1,
                        tableName));
        }

        public static List<ITableReplacementSite> GetTablesWithoutAlias(
            int minIdx,
            params string[] names)
        {
            return
                names
                .Select(
                    (x,i) => GetTableWithoutAlias(minIdx+ (100 * i), x))
                .Cast<ITableReplacementSite>()
                .ToList();
        }

        public static TableReplacementSiteMockup GetTableWithAlias(
            int startIdx,
            string tableName,
            string alias)
        {
            return
                new TableReplacementSiteMockup(
                    new IdentifierReplacementSiteMockup(
                        startIdx+1,
                        tableName),
                    alias: new IdentifierReplacementSiteMockup(
                        startIdx+tableName.Length+2,
                        alias));
        }

        public static ColumnReplacementSiteMockup GetColumnWithoutTable(
            int startIdx,
            string columnName)
        {
            return
                new ColumnReplacementSiteMockup(
                    new IdentifierReplacementSiteMockup(
                        startIdx, columnName));
        }

        public static ColumnReplacementSiteMockup GetColumnWithTable(
            int startIdx,
            string tableName,
            string columnName)
        {
            return
                new ColumnReplacementSiteMockup(
                    new IdentifierReplacementSiteMockup(
                        startIdx+tableName.Length+2,
                        columnName),
                    table: new IdentifierReplacementSiteMockup(
                        startIdx+1,
                        tableName));
        }
    }
}
