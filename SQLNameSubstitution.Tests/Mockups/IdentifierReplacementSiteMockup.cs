using System;

namespace SQLNameSubstitution.Tests
{
    public class IdentifierReplacementSiteMockup : IIdentifierReplacementSite
    {
        public IdentifierReplacementSiteMockup(
            int startIdx,
            string originalName,
            string? originalUnquotedName = null)
        {
            StartIdx = startIdx;
            Length = originalName.Length;
            OriginalName = originalName;
            OriginalUnquotedName = originalUnquotedName??originalName;
        }

        public int StartIdx {get;}

        public int Length {get;}

        public string OriginalName {get;}

        public string OriginalUnquotedName {get;}

        public string GetDebugDescription()
        {
            return $"[{OriginalUnquotedName}]";
        }
    }
}
