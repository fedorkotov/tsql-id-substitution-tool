using System;
using System.Collections.Generic;

namespace SQLNameSubstitution.Tests
{
    public class ColumnReplacementSiteMockup : IColumnReplacementSite
    {
        public ColumnReplacementSiteMockup(
            IIdentifierReplacementSite columnName,
            IIdentifierReplacementSite? table = null)
        {
            Table = table;
            ColumnName = columnName;
        }

        public IIdentifierReplacementSite? Table {get;}

        public IIdentifierReplacementSite ColumnName {get;}
    }
}
