using System;

namespace SQLNameSubstitution.Tests
{
    public class TableReplacementSiteMockup : ITableReplacementSite
    {
        public TableReplacementSiteMockup(
            IIdentifierReplacementSite tableName,
            IIdentifierReplacementSite? alias = null)
        {
            Alias = alias;
            TableName = tableName;
        }

        public IIdentifierReplacementSite? Alias {get;}

        public IIdentifierReplacementSite TableName {get;}
    }
}
