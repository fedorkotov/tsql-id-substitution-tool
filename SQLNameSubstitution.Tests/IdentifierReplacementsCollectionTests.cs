using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace SQLNameSubstitution.Tests
{
    public class IdentifierReplacementsCollectionTests
    {
        [Fact]
        public void ApplyingReplacements()
        {
            const string query = "  SELECT column1, column2  FROM table1";

            var replacements =
                new OrderedIdentifierReplacementSitesCollection();

            replacements.Add(
                new IdentifierReplacementSiteMockup(
                    query.IndexOf("column1"),
                    "column1"),
                "c1");

            replacements.Add(
                new IdentifierReplacementSiteMockup(
                    query.IndexOf("column2"),
                    "column2"),
                "c2");

            replacements.Add(
                new IdentifierReplacementSiteMockup(
                    query.IndexOf("table1"),
                    "table1"),
                "table1_replacement");

            Assert.Equal(
                "  SELECT c1, c2  FROM table1_replacement",
                replacements.Apply(query));
        }

        [Fact]
        public void OverlappingReplacementSites()
        {
            const string query = "abracadabra";

            var replacements =
                new OrderedIdentifierReplacementSitesCollection();

            replacements.Add(
                new IdentifierReplacementSiteMockup(
                    query.IndexOf("abra"),
                    "abra"),
                "c1");

            replacements.Add(
                new IdentifierReplacementSiteMockup(
                    query.IndexOf("cadabra"),
                    "cadabra"),
                "c2");

            Assert.Throws<OverlappingReplacementSitesException>(
                () =>
                {
                    replacements.Add(
                        new IdentifierReplacementSiteMockup(
                            query.IndexOf("racadab"),
                            "racadab"),
                        "c3");
                });
        }
    }
}
