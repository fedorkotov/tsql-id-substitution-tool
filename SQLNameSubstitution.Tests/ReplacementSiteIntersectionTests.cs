using System;
using Xunit;

namespace SQLNameSubstitution.Tests
{
    public class IdentifierReplacementSite_IntersectionTests
    {
        [Fact]
        public void Conflict_SameStartIdx()
        {
            var replacement1 =
                new IdentifierReplacementSiteMockup(5, "Table1");

            var replacement2 =
                new IdentifierReplacementSiteMockup(5, "Table2");

            Assert.True(replacement1.Overlaps(replacement2));

        }

        [Fact]
        public void Conflict_Intersection()
        {
            var replacement1 =
                new IdentifierReplacementSiteMockup(5, "Table1");

            var replacement2 =
                new IdentifierReplacementSiteMockup(7, "Table2");

            Assert.True(replacement1.Overlaps(replacement2));

        }

        [Fact]
        public void Conflict_LastSymbol()
        {
            var replacement1 =
                new IdentifierReplacementSiteMockup(5, "Table1");

            var replacement2 =
                new IdentifierReplacementSiteMockup(10, "Table2");

            Assert.True(replacement1.Overlaps(replacement2));

        }

        [Fact]
        public void Conflict_FirstSymbol()
        {
            var replacement1 =
                new IdentifierReplacementSiteMockup(10, "Table1");

            var replacement2 =
                new IdentifierReplacementSiteMockup(5, "Table2");

            Assert.True(replacement1.Overlaps(replacement2));

        }

        [Fact]
        public void NoConflict_Left()
        {
            var replacement1 =
                new IdentifierReplacementSiteMockup(5, "Table1");

            var replacement2 =
                new IdentifierReplacementSiteMockup(11, "Table2");

            Assert.False(replacement1.Overlaps(replacement2));

        }

        [Fact]
        public void NoConflict_Right()
        {
            var replacement1 =
                new IdentifierReplacementSiteMockup(11, "Table1");

            var replacement2 =
                new IdentifierReplacementSiteMockup(5, "Table2");

            Assert.False(replacement1.Overlaps(replacement2));
        }
    }
}
