using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace SQLNameSubstitution.Tests
{
    public class ColumnWithCandidateTablesTests
    {
        [Fact]
        public void ColumnWithoutTable()
        {
            var columnWithTables =
                new ColumnReplacementSiteWithCandidateTables(
                    new TSQLIdentifierComparer(),
                    TestTools.GetColumnWithoutTable(10, "column1"),
                    TestTools.GetTablesWithoutAlias(100, "table1", "table2", "table3"));

            Assert.True(columnWithTables.TableNameIsEmptyOrAlias);

            string[] candidateTableNames =
                columnWithTables
                .TableCandidates
                .Select(x => x.TableName.OriginalUnquotedName)
                .ToArray();

            Assert.Equal(3, candidateTableNames.Length);
            Assert.Contains("table1", candidateTableNames);
            Assert.Contains("table2", candidateTableNames);
            Assert.Contains("table3", candidateTableNames);
        }

        [Fact]
        public void ColumnWithTable()
        {
            var columnWithTables =
                new ColumnReplacementSiteWithCandidateTables(
                    new TSQLIdentifierComparer(),
                    TestTools.GetColumnWithTable(10, "table2", "column1"),
                    TestTools.GetTablesWithoutAlias(100, "table1", "table2", "table3"));

            Assert.False(
                columnWithTables.TableNameIsEmptyOrAlias);

            string[] candidateTableNames =
                columnWithTables
                .TableCandidates
                .Select(x => x.TableName.OriginalUnquotedName)
                .ToArray();

            Assert.Single(candidateTableNames);
            Assert.Contains("table2", candidateTableNames);
        }

        [Fact]
        public void ColumnWithTableAliasName()
        {
            var columnWithTables =
                new ColumnReplacementSiteWithCandidateTables(
                    new TSQLIdentifierComparer(),
                    TestTools.GetColumnWithTable(10, "t2", "column1"),
                    new List<ITableReplacementSite>()
                    {
                        TestTools.GetTableWithAlias(100, "table1", "t1"),
                        TestTools.GetTableWithAlias(200, "table2", "t2"),
                        TestTools.GetTableWithoutAlias(300, "table3"),
                    });

            Assert.True(
                columnWithTables.TableNameIsEmptyOrAlias);

            string[] candidateTableNames =
                columnWithTables
                .TableCandidates
                .Select(x => x.TableName.OriginalUnquotedName)
                .ToArray();

            Assert.Single(candidateTableNames);
            Assert.Contains("table2", candidateTableNames);
        }

        [Fact]
        public void ColumnWithInvalidTable()
        {
            Assert.Throws<UnknownTableNameOrAliasException>(
                () => {
                new ColumnReplacementSiteWithCandidateTables(
                    new TSQLIdentifierComparer(),
                    TestTools.GetColumnWithTable(10, "table5", "column1"),
                    TestTools.GetTablesWithoutAlias(100, "table1", "table2", "table3"));});
        }

        [Fact]
        public void ColumnWithAmbiguousTableName()
        {
            Assert.Throws<DuplicateTableNamesException>(
                () => {
                new ColumnReplacementSiteWithCandidateTables(
                    new TSQLIdentifierComparer(),
                    TestTools.GetColumnWithTable(10, "table1", "column1"),
                    new List<ITableReplacementSite>()
                    {
                        TestTools.GetTableWithAlias(100, "table1", "t1"),
                        TestTools.GetTableWithAlias(100, "t1", "table1")
                    });});
        }


    }
}
