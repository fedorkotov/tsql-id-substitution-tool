using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace SQLNameSubstitution.Tests
{
    public class TSQLQueryMapTests
    {
        [Fact]
        public void SelectQueryIdentifierSubstitution()
        {
            const string query =
                "SELECT t1.Column1, Column2 "+
                "FROM Table1 AS t1 JOIN Table2 ON Table1ID=t1.ID";

            var queryMap =
                new TSQLQueryMap(
                    query,
                    new TSQLIdentifierComparer(),
                    new List<ITableReplacementSite>()
                    {
                        new TableReplacementSiteMockup(
                            new IdentifierReplacementSiteMockup(
                                query.IndexOf("Table1"),
                                "Table1"),
                            alias: new IdentifierReplacementSiteMockup(
                                query.IndexOf("t1 JOIN"),
                                "t1")),
                        new TableReplacementSiteMockup(
                            new IdentifierReplacementSiteMockup(
                                query.IndexOf("Table2"),
                                "Table2"))
                    },
                    new List<IColumnReplacementSite>()
                    {
                        new ColumnReplacementSiteMockup(
                            new IdentifierReplacementSiteMockup(
                                query.IndexOf("Column1"),
                                "Column1"),
                            table: new IdentifierReplacementSiteMockup(
                                query.IndexOf("t1"),
                                "t1")),
                        new ColumnReplacementSiteMockup(
                            new IdentifierReplacementSiteMockup(
                                query.IndexOf("Column2"),
                                "Column2")),
                        new ColumnReplacementSiteMockup(
                            new IdentifierReplacementSiteMockup(
                                query.IndexOf("Table1ID"),
                                "Table1ID")),
                        new ColumnReplacementSiteMockup(
                            new IdentifierReplacementSiteMockup(
                                query.IndexOf(".ID")+1,
                                "ID"),
                            table: new IdentifierReplacementSiteMockup(
                                query.IndexOf("t1.ID"),
                                "t1"))
                    });

                var dbSchema =
                    new DBSchema(
                        new List<ITableNameSubstitution>()
                        {
                            new TableNameSubstitution(
                                "Table1",
                                "TT1",
                                new List<NameSubstitution>()
                                {
                                    new NameSubstitution("ID", "TT1_ID"),
                                    new NameSubstitution("Column1", "TT1_Column1")
                                }),
                            new TableNameSubstitution(
                                "Table2",
                                "TT2",
                                new List<NameSubstitution>()
                                {
                                    new NameSubstitution("ID", "TT2_ID"),
                                    new NameSubstitution("Table1ID", "TT2_Table1ID"),
                                    new NameSubstitution("Column2", "TT2_Column2")
                                })
                        });

                Assert.Equal(
                    "SELECT t1.TT1_Column1, TT2_Column2 "+
                    "FROM TT1 AS t1 JOIN TT2 ON TT2_Table1ID=t1.TT1_ID",
                    queryMap.ReplaceNames(dbSchema));
        }

        [Fact]
        public void AmbiguousColumnNames()
        {
            const string query =
                "SELECT Column1 "+
                "FROM Table1 AS t1 JOIN Table2 ON Table1ID=t1.ID";

            var queryMap =
                new TSQLQueryMap(
                    query,
                    new TSQLIdentifierComparer(),
                    new List<ITableReplacementSite>()
                    {
                        new TableReplacementSiteMockup(
                            new IdentifierReplacementSiteMockup(
                                query.IndexOf("Table1"),
                                "Table1"),
                            alias: new IdentifierReplacementSiteMockup(
                                query.IndexOf("t1 JOIN"),
                                "t1")),
                        new TableReplacementSiteMockup(
                            new IdentifierReplacementSiteMockup(
                                query.IndexOf("Table2"),
                                "Table2"))
                    },
                    new List<IColumnReplacementSite>()
                    {
                        new ColumnReplacementSiteMockup(
                            new IdentifierReplacementSiteMockup(
                                query.IndexOf("Column1"),
                                "Column1"))
                    });

                var dbSchema =
                    new DBSchema(
                        new List<ITableNameSubstitution>()
                        {
                            new TableNameSubstitution(
                                "Table1",
                                "TT1",
                                new List<NameSubstitution>()
                                {
                                    new NameSubstitution("ID", "TT1_ID"),
                                    new NameSubstitution("Column1", "TT1_Column1")
                                }),
                            new TableNameSubstitution(
                                "Table2",
                                "TT2",
                                new List<NameSubstitution>()
                                {
                                    new NameSubstitution("Table1ID", "TT2_Table1ID"),
                                    new NameSubstitution("Column1", "TT2_Column2")
                                })
                        });

            Assert.Throws<AmbiguousColumnNameException>(
                () => queryMap.ReplaceNames(dbSchema));
        }
    }
}
