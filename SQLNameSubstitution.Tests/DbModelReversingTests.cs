using System;
using System.Collections.Generic;
using Xunit;

namespace SQLNameSubstitution.Tests
{
    public class DbModelReversingTests
    {
        [Fact]
        public void ReversingColumnNameSubstitution()
        {
            var column1 = new NameSubstitution("ID", "TT1_ID");
            var column1Reversed = column1.Reverse();

            Assert.Equal(
                column1.ReplacementName,
                column1Reversed.OriginalName);
            Assert.Equal(
                column1.OriginalName,
                column1Reversed.ReplacementName);
        }

        [Fact]
        public void ReversingTable()
        {
            var table1 =
                new TableNameSubstitution(
                    "Table1",
                    "TT1",
                    new List<NameSubstitution>()
                    {
                        new NameSubstitution("ID", "TT1_ID"),
                        new NameSubstitution("Column1", "TT1_Column1")
                    });
            var table1Reversed = table1.Reverse();

            Assert.Equal(
                table1.ReplacementName,
                table1Reversed.OriginalName);
            Assert.Equal(
                table1.OriginalName,
                table1Reversed.ReplacementName);

            Assert.True(table1Reversed.TryGetColumn(
                "TT1_ID",
                out INameSubstitution column1));
            Assert.Equal("TT1_ID", column1.OriginalName);

            Assert.True(table1Reversed.TryGetColumn(
                "TT1_Column1",
                out INameSubstitution column2));
            Assert.Equal("TT1_Column1", column2.OriginalName);
        }

        [Fact]
        public void ReversingDBSchema()
        {
            var reversedDbModel =
                new DBSchema(
                    new List<ITableNameSubstitution>()
                    {
                        new TableNameSubstitution(
                            "Table1",
                            "TT1",
                            new List<NameSubstitution>()),
                        new TableNameSubstitution(
                            "Table2",
                            "TT2",
                            new List<NameSubstitution>())
                    })
                .Reverse();


            Assert.True(reversedDbModel.TryGetTable(
                "TT1",
                out ITableNameSubstitution table1));
            Assert.Equal("TT1", table1.OriginalName);

            Assert.True(reversedDbModel.TryGetTable(
                "TT2",
                out ITableNameSubstitution table2));
            Assert.Equal("TT2", table2.OriginalName);
        }

        [Fact]
        public void DuplicateReversedSchemaColumnNames()
        {
            Assert.Throws<DuplicateColumnNamesException>(
                () =>
                {
                    new TableNameSubstitution(
                        "Table1",
                        "TT1",
                        new List<NameSubstitution>()
                        {
                            new NameSubstitution("ID", "TT1_ID"),
                            new NameSubstitution("Column1", "TT1_ID")
                        })
                    .Reverse();
                });
        }

        [Fact]
        public void DuplicateReversedSchemaTableNames()
        {
            Assert.Throws<DuplicateTableNamesException>(
                () =>
                {
                    new DBSchema(
                        new List<ITableNameSubstitution>()
                        {
                            new TableNameSubstitution(
                                "Table1",
                                "TT2",
                                new List<NameSubstitution>()),
                            new TableNameSubstitution(
                                "Table2",
                                "TT2",
                                new List<NameSubstitution>())
                        })
                    .Reverse();
                });
        }
    }
}
