#!/usr/bin/env python3
import sys
import os
import pefile

# As of today (2020-10-25) dotnet sdk for Linux produces console 
# applications when building a C# project with OutputType=WinExe. 
# GUI applications that automatically open a console window and die 
# when it is closed are unfamiliar and confusing for average Windows 
# user. A simple modification of PE executable header is enough 
# to turn the program into proper GUI application. 

if __name__ == "__main__":    
    if(len(sys.argv) != 2):
        print("This script requires path to one executable file as an argument")
        sys.exit(1)
    
    exe_path = sys.argv[1]
    if(not os.path.isfile(exe_path)):
        print("Executable file '%s' not found" % exe_path)
        sys.exit(2)

    print("Converting '%s' to GUI executable" % exe_path)
    pe = pefile.PE(exe_path)
    pe.OPTIONAL_HEADER.Subsystem=2
    pe.write(filename=exe_path)