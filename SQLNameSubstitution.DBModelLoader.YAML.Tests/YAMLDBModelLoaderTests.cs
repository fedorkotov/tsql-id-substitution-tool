using System;
using Xunit;
using Tests.Common;

namespace SQLNameSubstitution.DBModelLoader.YAML.Tests
{
    public class UnitTest1
    {        
        [Fact]
        public void MissingDBModelVertion()
        {
            var loader = new YAMLDBModelLoader();

            Assert.Throws<DBModelVertionNotSpecifiedException>(
                () =>
                {
                    loader.LoadDBModelFromString(
                        @"tables:
                         | - name: test
                         |   columns:
                         |     - name: ID".TrimPipe());
                });
        }

        [Fact]
        public void UnsupportedDBModelVertion()
        {
            var loader = new YAMLDBModelLoader();

            Assert.Throws<DBModelVertionNotSupportedException>(
                () =>
                {
                    loader.LoadDBModelFromString(
                        @"db-model-version: 2.0
                         |tables:
                         | - name: test
                         |   columns:
                         |     - name: ID".TrimPipe());
                });
        }

        [Fact]
        public void MissingTableName()
        {
            var loader = new YAMLDBModelLoader();

            Assert.Throws<MissingTableNameException>(
                () =>
                {
                    loader.LoadDBModelFromString(
                        @"db-model-version: 1.0
                         |tables:
                         | - columns:
                         |     - name: ID".TrimPipe());
                });
        }

        [Fact]
        public void MissingColumnName()
        {
            var loader = new YAMLDBModelLoader();

            Assert.Throws<MissingColumnNameException>(
                () =>
                {
                    loader.LoadDBModelFromString(
                        @"db-model-version: 1.0
                         |tables:
                         | - name: Table1
                         |   columns:
                         |     - name-replacement: ID".TrimPipe());
                });
        }

        [Fact]
        public void TableNameWithoutReplacement()
        {
            var dbModel =
                new YAMLDBModelLoader()
                .LoadDBModelFromString(
                    @"db-model-version: 1.0
                     |tables:
                     | - name: Table1
                     |   columns:                        
                     |     - name: ID".TrimPipe());

            bool tableExists = 
                dbModel.TryGetTable(
                    "Table1", 
                    out ITableNameSubstitution table1);

            Assert.True(tableExists);
            Assert.Equal("Table1", table1.OriginalName);
            Assert.Equal("Table1", table1.ReplacementName);
        }

        [Fact]
        public void TableNameWithReplacement()
        {
            var dbModel =
                new YAMLDBModelLoader()
                .LoadDBModelFromString(
                    @"db-model-version: 1.0
                     |tables:
                     | - name: Table1
                     |   name-replacement: TT1
                     |   columns:                        
                     |     - name: ID".TrimPipe());

            bool tableExists = 
                dbModel.TryGetTable(
                    "Table1", 
                    out ITableNameSubstitution table1);

            Assert.True(tableExists);
            Assert.Equal("Table1", table1.OriginalName);
            Assert.Equal("TT1", table1.ReplacementName);
        }

        [Fact]
        public void ColumnNameWithoutReplacement()
        {
            var dbModel =
                new YAMLDBModelLoader()
                .LoadDBModelFromString(
                    @"db-model-version: 1.0
                     |tables:
                     | - name: Table1
                     |   columns:                        
                     |     - name: ID".TrimPipe());

            bool tableExists = 
                dbModel.TryGetTable(
                    "Table1", 
                    out ITableNameSubstitution table1);

            Assert.True(tableExists);
            bool columnExists =
                table1.TryGetColumn(
                    "ID",
                    out INameSubstitution column1);

            Assert.Equal("ID", column1.OriginalName);
            Assert.Equal("ID", column1.ReplacementName);
        }

        [Fact]
        public void ColumnNameWithReplacement()
        {
            var dbModel =
                new YAMLDBModelLoader()
                .LoadDBModelFromString(
                    @"db-model-version: 1.0
                     |tables:
                     | - name: Table1
                     |   columns:                        
                     |     - name: ID
                     |       name-replacement: IDD".TrimPipe());

            bool tableExists = 
                dbModel.TryGetTable(
                    "Table1", 
                    out ITableNameSubstitution table1);

            Assert.True(tableExists);
            bool columnExists =
                table1.TryGetColumn(
                    "ID",
                    out INameSubstitution column1);

            Assert.Equal("ID", column1.OriginalName);
            Assert.Equal("IDD", column1.ReplacementName);
        }

        [Fact]
        public void MultipleColumns()
        {
            var dbModel =
                new YAMLDBModelLoader()
                .LoadDBModelFromString(
                    @"db-model-version: 1.0
                     |tables:
                     | - name: Table1
                     |   columns:                        
                     |     - name: ID
                     |       name-replacement: IDD
                     |     - name: Column1
                     |     - name: Column2".TrimPipe());

            bool tableExists = 
                dbModel.TryGetTable(
                    "Table1", 
                    out ITableNameSubstitution table1);

            Assert.True(tableExists);
            Assert.True(table1.TryGetColumn("ID", out _));
            Assert.True(table1.TryGetColumn("Column1", out _));
            Assert.True(table1.TryGetColumn("Column2", out _));
        }

        [Fact]
        public void MultipleTables()
        {
            var dbModel =
                new YAMLDBModelLoader()
                .LoadDBModelFromString(
                    @"db-model-version: 1.0
                     |tables:
                     | - name: Table1
                     |   columns:                        
                     |     - name: ID
                     | - name: Table2
                     | - name: Table3".TrimPipe());
            
            Assert.True(dbModel.TryGetTable("Table1", out _));
            Assert.True(dbModel.TryGetTable("Table2", out _));
            Assert.True(dbModel.TryGetTable("Table3", out _));
        }
    }
}
