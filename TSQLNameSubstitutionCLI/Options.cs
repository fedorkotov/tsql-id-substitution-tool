using CommandLine;


namespace TSQLNameSubstitutionCLI
{
    public static partial class Program
    {
        class Options {
            [Option('i', "input-file",
             HelpText = "Input SQL query file path. If not specified input query will be read from stdin")]
            public string? InputQueryFilePath { get; set; }

            [Option('s', "db-schema-path",
             Required = true,
             HelpText = "YAML database schema file path")]
            public string? DbSchemaPath { get; set; }

            [Option('o', "output-file",
             HelpText = "Output file path. If not specified transformed SQL query will be written to stdout")]
            public string? OutputFilePath { get; set; }

            [Option('r', "reverse",
             Default = false,
             HelpText = "If present reverses name substitution direction of db schema (default = false)")]
            public bool ReversedSubstitutionDirection {get;set;}
        }
    }
}
