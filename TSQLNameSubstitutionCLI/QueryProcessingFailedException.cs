using System;
using SQLNameSubstitution;
using System.IO;
using SQLNameSubstitution.DBModelLoader.YAML;
using SQLNameSubstitution.TSQL;
using CommandLine;
using CommandLine.Text;
using System.Runtime.Serialization;

namespace TSQLNameSubstitutionCLI
{
    public class QueryProcessingFailedException : Exception
    {
        public QueryProcessingFailedException()
        {
        }

        public QueryProcessingFailedException(string? message) : base(message)
        {
        }

        public QueryProcessingFailedException(string? message, Exception? innerException) : base(message, innerException)
        {
        }
    }
}
