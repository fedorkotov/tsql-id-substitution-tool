﻿using System;
using SQLNameSubstitution;
using System.IO;
using SQLNameSubstitution.DBModelLoader.YAML;
using SQLNameSubstitution.TSQL;
using CommandLine;
using CommandLine.Text;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace TSQLNameSubstitutionCLI
{

    public static partial class Program
    {
        public static int Main(string[] args)
        {
            var parser =
                new Parser(
                    with =>
                    {
                        with.EnableDashDash = true;
                        with.MaximumDisplayWidth = 80;
                    });
            var optionsParsingResult =
                parser.ParseArguments<Options>(args);

            CommandLine.Parser.Default.ParseArguments<Options>(args)
            .WithParsed(_TransformQuery)
            .WithNotParsed(HandleParseError);

            return 0;
        }

        private static void _TransformQuery(Options options)
        {
            try
            {
                var dbSchema = _ReadDBSchema(options.DbSchemaPath);

                if(options.ReversedSubstitutionDirection)
                {
                    dbSchema = dbSchema.Reverse();
                }

                string inputQuery =
                    options.InputQueryFilePath == null?
                    _ReadAllStdIn():
                    _ReadInputQueryFile(options.InputQueryFilePath);

                string transformedQuery =
                    new TSQLNameSubstitutionManager(
                        dbSchema,
                        new TSQLReplacementSitesExtractor())
                    .Replace(inputQuery);

                _WriteTransformedQuery(
                    options.OutputFilePath,
                    transformedQuery);
            }
            catch(QueryProcessingFailedException ex)
            {
                Console.WriteLine("ERROR: "+ex.Message);
            }
            catch(DuplicateColumnNamesException ex)
            {
                Console.WriteLine("ERROR: "+ex.Message);
            }
            catch(DuplicateTableNamesException ex)
            {
                Console.WriteLine("ERROR: "+ex.Message);
            }
            catch(AmbiguousColumnNameException ex)
            {
                Console.WriteLine("ERROR: "+ex.Message);
            }
        }

        private static void HandleParseError(IEnumerable<Error> errs)
        {

        }

        private static string _ReadAllStdIn()
        {
            using (var reader = new StreamReader(
                Console.OpenStandardInput()))
            {
                return reader.ReadToEnd();
            }
        }

        private static string _ReadInputQueryFile(string filePath)
        {
            if(!File.Exists(filePath))
            {
                throw new QueryProcessingFailedException(
                    $"input query file '{filePath}' does not exist");
            }

            return File.ReadAllText(filePath);
        }

        private static IDBSchema _ReadDBSchema(string? filePath)
        {
            if(filePath==null)
            {
                throw new QueryProcessingFailedException(
                    "db schema path not specified");
            }

            if(!File.Exists(filePath))
            {
                throw new QueryProcessingFailedException(
                    $"db schema file '{filePath}' does not exist");
            }

            return
                new YAMLDBModelLoader()
                .LoadDBModelFromFile(filePath);
        }

        private static void _WriteTransformedQuery(
            string? filePath,
            string transformedQuery)
        {
            if(filePath==null)
            {
                Console.Write(transformedQuery);
            }
            else
            {
                File.WriteAllText(filePath, transformedQuery);
            }
        }
    }
}
